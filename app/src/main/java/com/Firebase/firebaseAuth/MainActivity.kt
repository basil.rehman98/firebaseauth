package com.Firebase.firebaseAuth

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class MainActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    private val time: Long = 2000
    lateinit var fadeanim: Animation
    lateinit var tv: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        auth=Firebase.auth
//        FirebaseApp.initializeApp(this)
        tv = findViewById(R.id.tV_splash) as TextView
        fadeanim = AnimationUtils.loadAnimation(this, R.anim.fade_in)
        Handler().postDelayed({
            tv.startAnimation(fadeanim)
            finish()
//            fadeanim.start()
//            fadeanim.duration=time
            loadnextscreen()
        }, time,)


    }

    private fun loadnextscreen() {
//        tv.startAnimation(fadeanim)
//        fadeanim.hasEnded()
        moveLogin()
    }
    private fun moveLogin() {
        val i = Intent(this, LoginActivity::class.java)
        startActivity(i)
        overridePendingTransition(R.anim.slide_in,R.anim.slide_out)
    }
}
