package com.Firebase.firebaseAuth

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.facebook.login.widget.LoginButton
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FacebookAuthProvider
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class LoginActivity : AppCompatActivity() {
    lateinit var callbackManager: CallbackManager
    lateinit var mGoogleSignInClient :GoogleSignInClient
    val RC_SIGN_IN : Int = 7
    private lateinit var auth: FirebaseAuth
    var TAG="checkinggoogle"
//val auth: FirebaseAuth = FirebaseAuth.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FacebookSdk.sdkInitialize(applicationContext)
    if (BuildConfig.DEBUG) {
        FacebookSdk.setIsDebugEnabled(true);
        FacebookSdk.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);
    }
        setContentView(R.layout.activity_login)
        auth=Firebase.auth
        val  gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.default_web_client_id))
            .requestEmail()
            .build()
         mGoogleSignInClient = GoogleSignIn.getClient(this, gso)

        callbackManager = CallbackManager.Factory.create()

        val email = findViewById<EditText>(R.id.login_username_in)
        val password = findViewById<EditText>(R.id.login_pass_in)
        val btn1 = findViewById<Button>(R.id.signin)
        val btn2 = findViewById<TextView>(R.id.tV_forgepass)
        val btn3 = findViewById<TextView>(R.id.signup)
        val crdbtn1 = findViewById<com.google.android.gms.common.SignInButton>(R.id.googlelog_btn)
        val crdbtn2 = findViewById<LoginButton>(R.id.facebooklog_btn)

        btn1.setOnClickListener {
           val txt_email: String = email.text.toString()
            val txt_password : String = password.text.toString()
            loginUser(txt_email, txt_password)
        }
        btn2.setOnClickListener {
            openForgetpage()
        }
        btn3.setOnClickListener {
            openSignUp()
        }
        crdbtn1.setOnClickListener {
            googlelogin()
        }
        crdbtn2.setOnClickListener {
            facebooklogin()
        }
    }

    private fun facebooklogin() {
        LoginManager.getInstance().registerCallback(
            callbackManager,
            object : FacebookCallback<LoginResult> {
                override fun onSuccess(loginResult: LoginResult) {
                    // App code
                    handleFacebookAccessToken(loginResult.accessToken);
                }

                override fun onCancel() {
                    // App code
                }

                override fun onError(exception: FacebookException) {
                    // App code
                }
            })
    }

    private fun handleFacebookAccessToken(accessToken: AccessToken?) {
        Log.d(TAG, "handleFacebookAccessToken:" + accessToken)
        val credential = accessToken?.token?.let { FacebookAuthProvider.getCredential(it) }
        if (credential != null) {
            auth.signInWithCredential(credential)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        Log.d(TAG, "signInWithCredential:success")
                        val user = auth.currentUser
                        startActivity(Intent(this, MainActivity::class.java))
                    } else {
                        // If sign in fails, display a message to the user.
                        Log.w(TAG, "signInWithCredential:failure", task.getException())
                        Toast.makeText(
                            this, "Authentication failed.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
        }
    }

    private fun loginUser(txt_email: String, txt_password: String) {

        auth.signInWithEmailAndPassword(txt_email, txt_password)
            .addOnCompleteListener(this) { taskId ->
                if (taskId.isSuccessful) {
                    Toast.makeText(this, "Login Successful", Toast.LENGTH_SHORT).show()
                    openMenu()
                    finish()
                } else {
                    Toast.makeText(baseContext, "Authentication Failed", Toast.LENGTH_SHORT).show()
                }
            }
    }


    override fun onStart() {
        super.onStart()
        val user = FirebaseAuth.getInstance().currentUser
        if (user != null) {
            Toast.makeText(this, "already login", Toast.LENGTH_SHORT).show()
            startActivity(Intent(this, Home::class.java))
            finish()
        }
    }
    private fun googlelogin() {
        val signInIntent: Intent = mGoogleSignInClient.getSignInIntent()
        startActivityForResult(signInIntent, RC_SIGN_IN)
    }

        override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
            callbackManager.onActivityResult(requestCode, resultCode, data)
            super.onActivityResult(requestCode, resultCode, data)

            // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
            if (requestCode == RC_SIGN_IN) {
                val task = GoogleSignIn.getSignedInAccountFromIntent(data)
                try {
                    // Google Sign In was successful, authenticate with Firebase
                    val account = task.getResult(ApiException::class.java)!!
                    Log.e("checkid", "firebaseAuthWithGoogle:" + account.id)
                    firebaseAuthWithGoogle(account.idToken!!)
                } catch (e: ApiException) {
                    // Google Sign In failed, update UI appropriately
                    Log.e(TAG, "Google sign in failed", e)
                }
            }
        }

    private fun firebaseAuthWithGoogle(idToken: String) {
        val credential = GoogleAuthProvider.getCredential(idToken, null)
        auth.signInWithCredential(credential)
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    // Sign in success, update UI with the signed-in user's information
                    Log.e(TAG, "signInWithCredential:success")
                    val user = auth.currentUser
                    Toast.makeText(this, "welcome: " + user, Toast.LENGTH_SHORT).show()
                    startActivity(Intent(this, Home::class.java))
                } else {
                    Snackbar.make(View(this), "Authentication Failed.", Snackbar.LENGTH_SHORT).show()

                }
            }
    }

    private fun openSignUp() {
        val i = Intent(this, SignUp::class.java)
        startActivity(i)
    }

    private fun openForgetpage() {
        val i = Intent(this, ForgetPass::class.java)
        startActivity(i)
    }

    private fun openMenu() {
        val i = Intent(this, Home::class.java)
        startActivity(i)
    }

}
