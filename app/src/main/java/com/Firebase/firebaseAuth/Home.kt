package com.Firebase.firebaseAuth

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase

class Home : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)
        val btn = findViewById<Button>(R.id.logout_btn)
        btn.setOnClickListener {
            Firebase.auth.signOut()
            val i = Intent(this@Home, LoginActivity::class.java)
            startActivity(i)
        }
    }
}